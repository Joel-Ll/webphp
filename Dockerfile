FROM debian
RUN apt update
RUN apt install apache2 lib-apache2-mod-php php php-mysql -y
COPY web /var/www/html/
ENV MYSQL_HOST=localhost
ENV MYSQL_USERNAME=root
ENV MYSQL_PASSWORD=123
ENV MYSQL_DB=sistemas
EXPOSE 80
CMD ["/bin/bash","-c","set -e:apachectl -D FOREGROUND \"$@\"]